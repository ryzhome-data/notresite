---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
Title: Bienvenue
layout: page
---

Une des conditions préalables à la réussite des projets de transformation numérique est **la convergence des intérêts** entre les parties prenantes. Nous vous proposons de vous accompagner pour identifier ensemble les sources de valeurs qui correspondent aux intérêts de chacun.e et aux possibilités de contribution aux biens communs.

![mettre l'organisation sur les bons rails](images/convergenceRail.jpg)

Pour construire cette convergence nous proposons d'accompagner les parties prenantes dans l'identification de leurs intérêts à court, moyen et long terme en itérant sur la **vision commune à construire** et sur les moyens nécessaires pour y parvenir.

Au travers de notre expérience du fonctionnement des administrations publiques et de la conduite de projets de gestion du cycle de vie de l'information, nous vous proposons un accompagnement aussi bien en terme d'organisation qu'en terme de préconisations techniques.

## Points clés de notre offre

Depuis de nombreuses années, nous travaillons, collaborons, échangeons et argumentons autour de nos visions de la donnée, de son évolution et de la **nécessaire complémentarité des points de vue**. Le 
monde de la donnée permet à deux mondes autrefois dissociés de se connecter et notre collaboration est un exemple de fusion des domaines de compétences des archivistes et des informaticiens.

![Faire Rhizome](images/82321_iris_german.tif.jpg)

Nous pouvons donc vous accompagner dans de nombreux domaines que cela soit la mise en place d'un projet d'**archivage électronique**, d'un **portail documentaire ou culturel**, d'une **cartographie des flux de données** ou d'un portail **open data** et ou les 4 en même temps !

Nous connaissons le contexte réglementaire et nous pouvons vous accompagner dans sa mise en oeuvre. Nous proposons également de partir des usages pour donner de l'impact à vos projets.

## Mise en contexte

Pour comprendre le contexte de votre besoin, nous aimons bien nous imprégner de votre quotidien. Ne soyez donc pas surpris si vous nous retrouvez un matin près de la machine à café ou entrain de télétravailler sur un de vos bureaux.

![permaculture](images/permaculture.jpg)

Par l'identification des parcours usagers [](fiches/parcours-usagers.md), la réalisation d'ateliers de story mapping [](fiches/story-mapping.md) ou l'interview des parties prenantes, nous cherchons à comprendre et à reformuler les besoins exprimés afin de chercher avec vous la meilleure manière d'y répondre.

Nous pouvons intervenir sur un [vaste champs de projets](missions.html) pour apporter nos connaissances et nos méthodes de participation au service de la maîtrise de vos racines informationnelles.

Vous pouvez consulter notre [outillage](outillage.html), [notre approche](approche.html) et [nos compétences](competences.html) ainsi que lire nos [statuts](Statuts-Rhizome-data.html) associatifs.

[contact](mailto:contact@rhizome-data.fr)
