---
layout: formation
id: dataliteracy-intro
category: parcours-data
title: Les enjeux de la gestion du cycle de vie de la donnée
length: 1 jour (7 heures)
object: > 
    La donnée est partout et pourtant sa définition ne vas pas de soi. Afin de construire un socle commun de "dataliteracy", ce module se propose de considérer les données au sein d'un cycle allant de la création à la destruction en passant par les étapes courantes de production, structuration, modification, validation, pour comprendre les enjeux associés à leur partage, leur conservation et leur protection.
audience: >
     élu.es, décideurs.euses, chargés de mission.
prerequisites: > 
    aucun si ce n'est la curiosité :)
topics:
   - Qu'est ce qu'une donnée ?
   - Qui produit de la donnée ?
   - Comprendre le cadre réglementaire
   - Quels sont les usages de la donnée ?
   - Quels sont les enjeux ?
   - Quelles sont les compétences et les métiers associés ?

adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins.  
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - web
    - data
---

### Programme : 

1. Qu'est ce qu'une donnée ?
    1. un élément d'information structuré
    2. Les données de référence
    3. Les données de gestion
    4. Les données personnelles
2. Qui produit de la donnée ?
   1. Tout le monde
   2. Les capteurs
   3. Les services
3. Comprendre le cadre réglementaire
   1. Les lois et règlements en vigueur
   2. Les licences associées à la diffusion
4. Quels sont les usages de la donnée ?
   1. Le territoire et la carte connectés
   2. l'évaluation des politiques publiques
   3. La capacité à rendre compte
   4. Le service de la donnée
5. Quels sont les enjeux ?
   1. Valoriser l'action des acteurs publics
   2. Interagir avec les partenaires sur le territoire
   3. Protéger la vie privée
6. Quelles sont les compétences et les métiers associés ?
