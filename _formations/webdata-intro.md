---
layout: formation
id: webdata-intro
category: webdata
title: Intitiation au web de données pour les archives
length: 3 jours (21 heures)
object: > 
    amé­lio­rer la com­pré­hen­sion des concepts du web de don­nées (normes et stan­dards, voca­bu­laire, champs d’appli­ca­tion), appré­hen­der la cons­truc­tion des voca­bu­lai­res ou des listes d’auto­ri­tés en SKOS, appren­dre à connaî­tre ce stan­dard, connaî­tre les évolutions nor­ma­ti­ves et les pers­pec­ti­ves de mise en appli­ca­tion (RIC : « Record In Context »).
audience: >
    Ce cours s'adresse aux archi­vis­tes en charge de pro­jets de valo­ri­sa­tion, e-archi­vis­tes, infor­ma­ti­ciens en charge de pro­jets liés aux archi­ves.
prerequisites: > 
    maî­tri­ser les fonc­tion­na­li­tés de base du web, connaî­tre les for­mats de don­nées et les stan­dards de des­crip­tion (XML, EAD, EAC, SEDA.
topics:
    - Présentation des concepts du web
    - Présentation des concepts du web séman­ti­que, le cadre de des­crip­tion RDF, les iden­ti­fiants des res­sour­ces
    - Définition du concept d’onto­lo­gie
    - Présentation du stan­dard SKOS
    - Présentation de la notion de concept et des rela­tions pos­si­bles entre les concepts
    - Panorama des res­sour­ces dis­po­ni­bles (jeux de don­nées, outils)
    - L’usage du web séman­ti­que dans un projet d’archi­vage électronique
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins. Elle a notamment été organisée en partenariat avec l'association des archivistes français pour un public intéressé par l'utilisation du web de données pour la gestion des archives numériques. Elle peut être adpatée aux besoins spécifiques d'une entreprise ou d'une organisation publique oeuvrant dans un domaine d'activité différent. 
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - web
    - data
---

### Sessions

Cette formation a été organisée en partenariat avec l'association des archivistes français en 2019. Les slides de cette formation organisée en collaboration avec Logilab sont disponibles [ici](https://rhizome-data.gitlab.io/formations/presentations/aaf/webData/formationAAF.html#1)

Une nouvelle session est planifiée en septembre 2020 : les inscriptions sont accessible sur le [site de l'AAF](https://archivistes.org/Mettre-en-oeuvre-le-web-semantique-dans-la-description-archivistique-stage)