---
layout: formation
id: versement-ae
category: archivage électronique
title: Mise en oeuvre du versement d'archives électroniques
length: 2 jours (14 heures)
object: > 
    En complément des flux sériels versés au travers de connecteurs applicatifs, les versements d'archivage électronique concernent également la production semi-structurée de type bureautique ou boîtes mail. Les outils Resip et Archifiltre permettent la production de bordereaux de versement normalisés.
audience: >
    Ce cours s'adresse aux personnes souhaitant mettre en oeuvre des versements de paquets d'archives issus de la production bureautique.
prerequisites: > 
    Connaissance du SEDA et des processus d'archivage papier.
topics:
    - Le standard de description SEDA (durée 1 jour) 
        - Présentation du modèle OAIS (entités, processus et paquets d'information)
        - Présentation du standard SEDA et des standards associés (EAC, SKOS)
        - Cas d'usages
    - Mise en oeuvre des outils (durée  1 jour) 
        - Analyse et description d'une arborescence documentaire avec Archifiltre
        - Production de SIP avec Resip
        - Utilisation des outils associés (SHERPA, Octave, Vitam)
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins. Rhizome-data dispose aujourd'hui d'une palette très large de modules pédagogiques permettant la personnalisation de ses formations. L'adaptation des cours en intra-entreprise ne pose aucun problème ; cette opération est cependant plus difficile pour les cours en inter-entreprises. N'hésitez pas à demander conseil à notre service commercial
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - archivage
    - standard
---