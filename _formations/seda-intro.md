---
layout: formation
id: seda-intro
category: archivage électronique
title: Mise en oeuvre du standard SEDA
length: 2 jours (14 heures)
object: > 
    Le standard d'échanges des données d'archivage (SEDA) permet de normaliser les processus d'archivage électronique et de garantir la traçabilité des échanges entre producteurs et services d'archives.
audience: >
    Ce cours s'adresse aux personnes souhaitant maîtriser les normes et processus de l'archviage électronique et les outils de production de bordereau SEDA.
prerequisites: > 
    Connaissance des standards de description (EAD, EAC) et des languages de structuration (XML, CSV, JSON).
topics:
    - Le modèle de données du SEDA (durée 1 jour) 
        - Présentation du protocole d'échange (acteurs, messages et propriétés)
        - Outillage et mise en oeuvre
        - Enjeux et prespectives
    - Mise en pratique de la structuration de bordereaux SEDA (durée  1 jour) 
        - Construction d'un profil d'archivage
        - Production de notices producteur et de vocabulaires contrôlés
        - Construction de bordereaux de versement
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins. Rhizome-data dispose aujourd'hui d'une palette très large de modules pédagogiques permettant la personnalisation de ses formations. L'adaptation des cours en intra-entreprise ne pose aucun problème ; cette opération est cependant plus difficile pour les cours en inter-entreprises. N'hésitez pas à demander conseil à notre service commercial
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - archivage
    - standard
---