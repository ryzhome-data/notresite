---
layout: formation
id: dataliteracy-part3
category: parcours-data
title: La (ré-)utilisation de la donnée
length: 2 jours (14 heures)
object: > 
    Ce module aborde les données comme un média et dresse un panorama des outils et techniques permettant de disposer d'un regard critique sur les utilisations qui peuvent en être faites.
audience: >
    tout public
prerequisites: > 
    culture de la donnée, connaissance des usages de la données.
topics:
    - sémiologie de la représentation de donnée
    - Datavisualisation
    - Alignement et mise en relation
    - modèles prédictifs
    - consommer des données en programmant
    - aspects juridiques : cadre réglementaire et enjeux sociaux
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins.
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - web
    - parcours-data
---

### Programme

1. sémiologie de la représentation de donnée
2. Datavisualisation
3. Alignement et mise en relation
4. modèles prédictifs
5. consommer des données en programmant
6. aspects juridiques : cadre réglementaire et enjeux sociaux