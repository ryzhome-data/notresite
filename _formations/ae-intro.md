---
layout: formation
id: ae-intro
category: archivage électronique
title: Mise en oeuvre de l'archivage électronique
length: 2 jours (14 heures)
object: > 
    Les processus d'archivage suivent le mouvement de dématérialisation des processus administratifs. Leur mise en oeuvre s'attache à adresser les besoins de valeur probante et de conservation pérenne.
audience: >
    Ce cours s'adresse aux personnes souhaitant maîtriser les processus d'archivage électronique, les standards associés et la méthodologie de mise en oeuvre d'un système d'archivage électronique.
prerequisites: > 
    Connaissance des standards de description (EAD, EAC) et des processus d'archivage papier.
topics:
    - Le modèle normatif OAIS et les standards de description (durée 1 jour) 
        - Présentation du modèle OAIS (entités, processus et paquets d'information)
        - Outillage et mise en oeuvre
        - Enjeux et prespectives
    - Mise en place d'un système d'archivage électronique (durée  1 jour) 
        - Méthodologie projet et cas d'usages
        - Référentiels et vocabulaires contrôlés
        - outillage
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins. Rhizome-data dispose aujourd'hui d'une palette très large de modules pédagogiques permettant la personnalisation de ses formations. L'adaptation des cours en intra-entreprise ne pose aucun problème ; cette opération est cependant plus difficile pour les cours en inter-entreprises. N'hésitez pas à demander conseil à notre service commercial
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - archivage
    - standard
---