---
layout: formation
id: dataliteracy-part2
category: parcours-data
title: La production de la donnée
length: 2 jours (14 heures)
object: > 
    Ce module s'adresse aux gestionnaires ou experts de la données chargé de créer ou contribuer à des modèles de données. Il aborde les sujets des outils aussi bien en terme de format que de structure et s'attache à replacer ces activités au sein de l'écosystème du web comme système global de gestion et de navigation au sein de l'information
audience: >
    métiers de la donnée (gestion du patrimoine, culture, chargés d'étude, finances, communication)
prerequisites: > 
    aucun. Avoir suivi le [premier module du parcours](parcours-data-part1) peut constituer un plus en fonction du niveau de connaissance du sujet
topics:
    - Qu'est ce qu'un modèle de données ?
    - Qu'est-ce qu'un schéma de données ?
    - Les aspects de la qualité de la donnée
    - La contribution aux entrepôts publics (OSM, Wikidata, data.gouv.fr)
    - Les outils
    - visualisation de données
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins.
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - web
    - parcours-data
---

### Programme


1. Qu'est ce qu'un modèle de données ?
2. Qu'est-ce qu'un schéma de données ?
3. Les aspects de la qualité de la donnée
4. La contribution aux entrepôts publics (OSM, Wikidata, data.gouv.fr)
5. Les outils
   1. produire un fichier csv : bonnes pratiques
   2. nettoyer des données avec OpenRefine
   3. concevoir et alimenter une base de données
   4. produire une carte
      1. géocoder
      2. décrire
      3. sémiologie de la représentation
   5. visualisation de données
      1. rapports
      2. interaction
      3. modèles de représentation