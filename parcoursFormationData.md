---
layout: page
title: Catalogue de formation
---

# La permaculture de la donnée au service du public

Bien que les outils numériques se soient multipliés au sein des administrations publiques, au point que la plupart des actes de gestion et des interactions avec les citoyens utilisent ce canal de production, les enjeux du pilotage de l’information (données et documents) n’y sont que partiellement adressés.  

Le partage de documents, la multiplication des interfaces de gestion dématérialisées et la structuration des données dans des bases de données, offrent des possibilités accrues de communication et de collaboration.  

![la perma-culture des données](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Mandala_permaculture.jpg/800px-Mandala_permaculture.jpg)

Ces modalités de gestion impliquent toutefois une évolution des pratiques et une mise en conformité vis-à-vis des réglementations en matière de gestion de la sécurité et de la protection des données personnelles.

Il nous paraît nécessaire d'accompagner les producteur.rice.s du service public dans la compréhension, la connaissance voire la maîtrise des aspects liés à réception, la (ré)-utilisation, la production et la diffusion de données.

La confiance et la distribution des compétences qui en résulteront sont de nature à permettre une gouvernance partagée des services publics de la donnée qui restent à construire et au développement d'une transformation numérique pilotée par les usage(r)s.

## Présentation

Assister à une formation Rhizome-data, c'est :

* apprendre avec des professionnels compétents maîtrisant parfaitement le sujet qu'ils enseignent,
* bénéficier des conseils et des avis d'experts travaillant depuis longtemps dans le domaine,
* trouver des interlocuteurs attentifs et disponibles, cherchant à apporter à chacun les informations dont il a besoin.

Confrontés quotidiennement à ces domaines de connaissance, ils ont acquis une réelle expertise sur ces sujets, 
ce qui leur offre la possibilité de les enseigner dans un esprit pragmatique. Les experts choisis pour animer une formation ont tous,  
outre leur excellence technique, une expérience notable dans le domaine de la formation professionnelle. 


## Modalités

Toutes nos formations peuvent avoir lieu en intra-entreprise au lieu de votre convenance en France ; 

Rhizome-data a choisi de systématiquement adapter ses formations au plus près des besoins de chacun de ses clients. 
Ainsi, vous pouvez soit choisir une de nos formations standards décrites ci-après, 
soit nous contacter pour composer une formation sur-mesure. 

## Formation à la carte

À partir de 4 personnes, Rhizome-data peut animer des formations spécifiques en intra-entreprise, n'importe où en France.

Vous pouvez alors composer la formation répondant exactement à vos besoins. 

Nous sommes à votre disposition pour vous conseiller et vous aider dans vos choix.

## Formations DIF

Depuis la mise en place du droit individuel à la formation, nous proposons une offre spécifique pour les personnes souhaitant 
consacrer leur crédit formation à l'entretien ou l'amélioration de leurs compétences informatiques. 

Ces formations reprennent les thèmes de notre [catalogue](/formations), mais sont organisées sur des périodes plus courtes (de préférence trois jours) et peuvent se dérouler pour partie en alternance et à distance, de manière à s'adapter au mieux aux contraintes d'emploi du temps et de budget des participants. 

Nous sommes à votre disposition pour vous présenter en détail cette offre.

## Offre du catalogue

Vous pouvez consulter notre catalogue de formation sur cette [page](/formations).