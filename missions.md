---
layout: page
title: Offres de service
---

Nous pouvons intervenir à la fois, tant sur des **assistances à maîtrise d'ouvrage** que sur de la **maîtrise d'ouvrage déléguée**. Notre objectif est de vous transmettre nos savoir-faire pour **renforcer votre expertise** mais si vous ne disposez pas des moyens humains pour mener à bien vos projets nous pouvons vous accompagner dans le **suivi opérationnel**. Nous sommes également agréés comme organisme de formation et nous proposons des prestations de **formation-action** qui permettent de valider le besoin collectivement.

![types de mission](images/missionengel-leonardo-antillas-2023.jpg)

### Assistance à maîtrise d'ouvrage

- Accompagnement à la **transformation numérique** (open data, qualité de la donnée et bonnes pratiques, RGPD, stratégie à mettre en oeuvre autour de la donnée).
- Accompagnement à la gestion du **cycle de vie** de l'information (archivage électronique, GED).
- Accompagnement à la **valorisation des archives** (site internet, rétroconversion des instruments de recherche, rétroconversion des notices producteurs, constitution de référentiels).
- Accompagnement à l'**informatisation** des services archives.
- Accompagnement à la **mise en oeuvre de la fonction archives** dans l'entreprise, l'association...

### Gouvernance de projets OpenData

- cartographie des flux
- constitution du répertoire d'informations publiques
- accompagnement, animation et offre de service

### Traitement des archives papiers et électroniques

- traitement d'archives privées (élaboration de tableaux de gestion, pré-classement, élaboration d'inventaire)
- modélisation des flux
- rédaction de profils de versements SEDA
- mapping
- gestion de projet de valorisation numérique

### Formations

<a href="formations">Consultez notre offre de formation</a>
